source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0"
# Use postgresql as the database for Active Record
gem "pg", "~> 1.2"
gem "activerecord-postgres_enum"
gem "activerecord-postgis-adapter"
gem "rgeo-geojson"
# Use Puma as the app server
gem "puma", "~> 5.0"
gem "pundit"
gem "propshaft"
gem "dartsass-rails", "~> 0.4.0"
gem "turbo-rails"
gem "hotwire-rails"
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder", "~> 2.7"
# Use Redis adapter to run Action Cable in production
gem "redis", "~> 4.0"
# Use Active Model has_secure_password
# gem "bcrypt", "~> 3.1.7"
gem "importmap-rails"
# Use Active Storage variant
# gem "image_processing", "~> 1.2"
gem "rubyzip"
# Used for authentification
gem "devise"
gem "devise_invitable"
gem "http_accept_language"
gem "rails-i18n", "~> 7.0"
gem "skylight"
gem "premailer-rails", github: "Intrepidd/premailer-rails", branch: "support-propshaft" # Waiting for https://github.com/fphilipe/premailer-rails/pull/266

gem "foreman", "~> 0.87.2"

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", ">= 1.4.4", require: false

group :development, :test do
  # Call "byebug" anywhere in the code to stop execution and get a debugger console
  gem "byebug", platforms: [:mri, :mingw, :x64_mingw]
  gem "standard"
  gem "rubocop-rails", require: false
  gem "rubocop-minitest", require: false
  # Verifies that we have all translations
  gem "i18n-tasks", "~> 1.0.10"
  gem "erb_lint", require: false
end

group :development do
  # Access an interactive console on exception pages or by calling "console" anywhere in the code.
  gem "web-console", ">= 4.1.0"
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem "rack-mini-profiler", "~> 2.0"
  gem "listen", "~> 3.3"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "annotate"
  gem "rails-erd"
  gem "letter_opener_web"
end

group :test do
  # Cuprite is used for system tests (to pilot chrome driver)
  gem "cuprite"
  gem "minitest-reporters"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]
